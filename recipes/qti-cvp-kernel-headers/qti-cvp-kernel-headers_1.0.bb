inherit qprebuilt autotools-brokensep pkgconfig
HOMEPAGE         = "https://git.codelinaro.org/"
DESCRIPTION = "Generates qti_cvp_kernel headers"

LICENSE          = "GPL-2.0 WITH Linux-syscall-note"
LIC_FILES_CHKSUM = "file://${WORKSPACE}/kernel-5.15/kernel_platform/msm-kernel/COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

PACKAGE_ARCH    ?= "${MACHINE_ARCH}"

FILESPATH = "${WORKSPACE}/:"
SRC_URI   = "file://vendor/qcom/opensource/cvp-kernel/include/uapi/eva/media"

S = "${WORKDIR}//vendor/qcom/opensource/cvp-kernel/include/uapi/eva/media"

ALLOW_EMPTY_${PN} = "1"