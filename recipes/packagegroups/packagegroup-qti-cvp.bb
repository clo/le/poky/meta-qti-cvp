SUMMARY = "QTI CVP Package Group"

LICENSE = "BSD-3-Clause-Clear"
PACKAGE_ARCH = "${MACHINE_ARCH}"
inherit packagegroup

PROVIDES = "${PACKAGES}"

PACKAGES = "\
    packagegroup-qti-cvp \
    "
RDEPENDS:packagegroup-qti-cvp = " \
    cvp-kernel \
    "

